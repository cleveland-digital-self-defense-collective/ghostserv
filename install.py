import os
import sys
import time
import subprocess
import fileinput

title = '''



   ________               __  _____
  / ____/ /_  ____  _____/ /_/ ___/___  ______   __
 / / __/ __ \/ __ \/ ___/ __/\__ \/ _ \/ ___/ | / /
/ /_/ / / / / /_/ (__  ) /_ ___/ /  __/ /   | |/ /
\____/_/ /_/\____/____/\__//____/\___/_/    |___/  dev edition

A Reasonable Simple Way to Set Up Ephemeral Hidden Services
for Non-Technical Users

Installation Script \n'''

print(title)

print('[*]Please Hang Around While This Runs...')
print('Interaction With Script Is Necessary')

def download():
    print('[+] Installing from Repository')
    os.system('sudo apt update')
    os.system('sudo apt install tor nodejs npm python3-pip python-pip')
    print('\n')
    print('[+] Installing Python Packages')
    print('\n')
    os.system('sudo pip3 install stem')
    os.system('sudo pip3 install termcolor')
    os.system('os.system pip3 install fileinput')
    print('\n')
    print('[+] Installing Simple-File-Server')
    os.system('git clone https://github.com/RDCH106/Simple-File-Server.git')
    root_dir = os.getcwd()
    print('\n')
    print('[+] Initial Configuration')
    os.chdir('Simple-File-Server')
    os.system('python simple_file_server.py')
    os.chdir(root_dir)
    print('\n')
    print('[+] Installing hirc')
    print('\n')
    os.system('git clone https://github.com/fboender/hircd.git')
    print('\n')
     print('\n')
    os.system('git clone https://github.com/prawnsalad/KiwiIRC.git')
    root_dir=os.getcwd()
    os.chdir('KiwiIRC')
    os.system('npm install')
    print('[+] Configuring Webchat Client')
    print('\n')
    #os.system('cp config.example.js config.example.js')
    line1 = '//conf.restrict_server = "irc.kiwiirc.com";'
    line2 = '//conf.restrict_server_port = 6667;'
    line3 = '//conf.restrict_server_ssl = false;'
    line1_replace = 'conf.restrict_server = "127.0.0.1";'
    line2_replace = 'conf.restrict_server_port = 6667;'
    line3_replace = 'conf.restrict_server_ssl = false;'

    def replaceAll(file, search_expression, replacement_expression):
        for line in fileinput.input(file, inplace=1):
            if search_expression in line:
                line = line.replace(search_expression, replacement_expression)
            sys.stdout.write(line)

    replaceAll("config.example.js", line1, line1_replace)
    replaceAll("config.example.js", line2, line2_replace)
    replaceAll("config.example.js", line3, line3_replace)
    os.system('mv config.example.js config.js')
    print('\n')
    print('[+] Building Webchat Application')
    os.system('./kiwi build')
    os.chdir(root_dir)
    print('\n')
    print('[*] Finished Installing Applications')

def tor_configure():
    #os.system('sudo apt update')
    #os.system('sudo apt install tor')
    print('\n')
    #control_port_passwd = input('Select Password For Tor Control Port: ')
    #print('\n')
    #print(colored('[*] Remember This Password', 'yellow', attrs=['bold']))
    #print('\n')
    #print(colored('[+]' + control_port_passwd, 'yellow', attrs=['bold']))
    #print('\n')
    #time.sleep(.2)
    print('[+] Copying Tor Configuration File')
    print('\n')
    os.system('sudo cp /etc/tor/torrc torrc')
    time.sleep(.2)
    #print('[+] Hashing Password')
    #print('\n')
    #command = "tor --hash-password " + control_port_passwd
    #hashed_password = subprocess.check_output(command, shell=True)
    #capture output of hashed password command as variable to pipe back in
    #pass_string = str(hashed_password)
    #print('[+] Hashed Control Port Password: ' + pass_string)
    #hash_passwd = pass_string.replace("b'", '')
    #final_hash = hash_passwd[:-3]
    #print(final_hash)
    #time.sleep(.2)
    #figure out how to rewrite these lines reliably by grabbing output of command and sending to file
    print('[+] Modifying Tor Configuration File to Activate Ephemeral Hidden Services')
    with fileinput.FileInput('torrc', inplace=True, backup='.bak') as config:
        for line in config:
            print(line.replace('#ControlPort 9051', 'ControlPort 9051'), end='')
    #        print(line.replace('#HashedControlPassword 16:872860B76453A77D60CA2BB8C1A7042072093276A3D701AD684053EC4C', 'HashedControlPassword ' + final_hash), end='')

    os.system('sudo mv /etc/tor/torrc /etc/tor/torrc.old')
    os.system('sudo mv torrc /etc/tor/torrc')
    os.system('sudo chown root:root /etc/tor/torrc')
    os.system('rm torrc.bak')




download()
tor_configure()
print('[+] Installation Complete')

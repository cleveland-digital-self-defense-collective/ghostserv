from stem.control import Controller
import os
from termcolor import colored
import time
import fileinput

def run():
    print(colored('[*] Starting IRC Server:', 'yellow', attrs=['bold']))
    print('\n')
    time.sleep(.5)
    os.system('python hircd/hircd.py --start --a 127.0.0.1 -p 6667')
    #password = input(colored('Tor Control Port Password: ', 'yellow'))
    print('\n')
    time.sleep(.5)
    print(colored('[*] Starting Webchat Client', 'yellow', attrs=['bold']))
    print('\n')
    time.sleep(.5)
    os.system('./KiwiIRC/kiwi start')
    print('\n')
    time.sleep(.5)
    print(colored('[*] Connecting to tor', 'yellow', attrs=['bold']))
    print('\n')

    with Controller.from_port() as controller:
        controller.authenticate()

        response = controller.create_ephemeral_hidden_service({80: 7778}, await_publication=True)
        print(colored('[*] Service available at %s.onion' % response.service_id, 'green', attrs=['bold']))
        print('\n')
        input(colored('[-] Press Any Key to Shut Down Hidden Service...', 'red', attrs=['bold']))
        controller.remove_ephemeral_hidden_service(response.service_id)
        print('\n')
        print(colored('[-] Hidden Service Stopping...', 'red', attrs=['bold']))

    print('\n')
    input(colored('[-] Press Enter To Stop IRC Server', 'red', attrs=['bold']))
    time.sleep(.2)
    print('\n')
    print(colored('[-]Stopping IRC Server', 'red', attrs=['bold']))
    print('\n')
    os.system('./KiwiIRC/kiwi stop')
    os.system('python hircd/hircd.py --stop')
    print('\n')

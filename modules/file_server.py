import json
from contextlib import contextmanager
import os
from stem.control import Controller
from termcolor import colored
import time

def run():
    print('\n')
    print(colored('[+] Set Username and Password for Service Access', 'yellow', attrs=['bold']))
    print('\n')
    username = input(colored('Username: ', 'cyan'))
    password = input(colored('Password: ', 'cyan'))
    prevdir = os.getcwd()
    #print('Current Directory: ' + prevdir)
    os.chdir('Simple-File-Server')
    time.sleep(0.5)
    print('\n')
    print(colored('[*] Modifying Configuration File', 'yellow', attrs=['bold']))
    print('\n')
    print(colored('Username: ' + username, 'green'))
    print(colored('Password: ' + password, 'green'))
    print('\n')
    time.sleep(0.5)

    with open('config/config.json', 'r+') as config:
        settings = json.load(config)
        settings['username'] = username
        settings['password'] = password
        config.seek(0)
        json.dump(settings, config, indent=4)
        config.truncate()

    print(colored('[*] Connecting to tor', 'yellow', attrs=['bold']))
    time.sleep(0.5)
    print ('\n')
    print(colored('[*] Starting Hidden Service', 'yellow', attrs=['bold']))

    with Controller.from_port() as controller:

        controller.authenticate()

        response = controller.create_ephemeral_hidden_service({80: 5000}, await_publication=True)
        print(colored('[*] Service available at %s.onion' % response.service_id, 'green', attrs=['bold']))
        print('\n')
        print(colored('[*] Starting File Server...', 'yellow', attrs=['bold']))
        print('\n')
        print(colored('[-] Press Control-C To Stop File Server', 'red', attrs=['bold']))
        os.system('python simple_file_server.py')
        print('\n')
        input(colored('[-] Press Any Key to Shut Down Hidden Service...', 'yellow', attrs=['bold']))
        controller.remove_ephemeral_hidden_service(response.service_id)
        print('\n')
        print(colored('[-] Hidden Service Stopping...', 'red'))
        print('\n')


    #print('Current Directory: ' + os.getcwd())
    #os.chdir(prevdir)
    #print('Current Directory: ' + os.getcwd())

import modules.chat
import modules.file_server
import modules.web_chat
from termcolor import colored
import argparse

print(colored('''

  ___| |                |    ___|
 |     __ \   _ \   __| __|\___ \   _ \  __|\ \   /
 |   | | | | (   |\__ \ |        |  __/ |    \ \ /
\____|_| |_|\___/ ____/\__|_____/ \___|_|     \_/   

Framework To Launch Temporary, Ephemeral, Tor Hidden Services

A Tool Released By The Digital Freedom Initiative
digitalfreedominitiative.org
''', 'red', attrs=['bold']))

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--irc', help='Launch IRC Server on Port 6667', action='store_true')
parser.add_argument('-w', '--webchat', help='Launch WebChat', action='store_true')
parser.add_argument('-f', '--file_server', help='Launch File Sharing Server', action='store_true')

args = parser.parse_args()

irc = args.irc
webchat = args.webchat
file_server = args.file_server

if args.irc:
    modules.chat.run()
if args.webchat:
    modules.web_chat.run()
if args.file_server:
    modules.file_server.run()

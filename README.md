<h1>GhostServ</h1>


<h2>A Tool By The Cleveland Digital Self Defense Collective</h2>
<h4>https://cdsdc.org</h4>

GhostServ is a proof of concept tool which makes the process of setting up and
running ephemeral Tor Hidden Services easier for slightly more technical that
usual users.

Tor Hidden Services have gotten a lot of attention since the emergence of the
Silk Road.  The hidden service is a web service that runs within the Tor network.
As a result users of the service can only access the service through the use of Tor.

For all of its faults, and for all of the fear mongering around the 'dark web',
Tor is an important tool for journalists, dissidents, activists and the merely
privacy conscious.  However, Tor Hidden Services, for as difficult as they can
be to find, are discoverable, and like any service, are able to be breached.  
Even with these issues, which are no greater than any web service, the added
anonymity of use still holds a significant amount of promise for high risk users
specifically dissidents.

The primary issue with hidden services, like any service, is that building a
service, securing that service, protecting users and configuring networking
(port forwarding, DNS, etc) can be difficult for the average or slightly
above average user.  

GhostServ attempts to be a proof of concept for a potential approach to alleviating
this issue.  It leverages the anonymity of the hidden service, but configures the
service to run as an ephemeral hidden service.  The ephemeral hidden service
protocol, which can be leveraged through the Tor Project developed Python library
Stem, allows a user to set up a hidden service that runs entirely in memory, with
no traces left of the service after the service is shut down.

The goal with this initial alpha version is not to build bullet-proof services,
if such a thing exists to begin with.  Rather, the goal is to demonstrate a concept,
the use of temporary, ephemeral, asymmetric technical infrastructure and the ability
to utilize scripts to simplify deployment.  Future development will include
a GUI of some sort, as well as more services.  If you have suggestions for a
service, or would like to contribute, please file an issue or a pull request.

GhostServ automates the building and running of modular services as ephemeral
hidden services, with an easy to use command line interface.  Currently the
framework includes modules for a simple IRC server, a file sharing web application,
and a webIRC. It also includes an installation script which will download, build
and configure the services.

To run GhostServ follow these steps:

`git clone https://git.digitalfreedominitiative.org/GhostServ/ghostserv.git`

`cd GhostServ`

`python3 install.py`

`sudo python3 ghostserv.py [option]`

The options are as follows:      
-i: IRC Server      
-f: File Sharing Application      
-w: webIRC

Note: This is only a proof of concept script.  These services have been
chosen due to simplicity, rather than security.  
